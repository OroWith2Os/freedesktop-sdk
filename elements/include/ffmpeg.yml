build-depends:
- components/nasm.bst
- components/vulkan-headers.bst
- public-stacks/buildsystem-autotools.bst

depends:
- bootstrap-import.bst
- components/dav1d.bst
- components/fontconfig.bst
- components/freetype.bst
- components/fribidi.bst
- components/libfdk-aac.bst
- components/lame.bst
- components/libva.bst
- components/libvdpau.bst
- components/librsvg.bst
- components/mpg123.bst
- components/openal.bst
- components/libpulse.bst
- components/sdl2.bst
- components/aom.bst
- components/gnutls.bst
- components/speex.bst
- components/ladspa-sdk.bst
- components/lcms.bst
- components/libtheora.bst
- components/libgcrypt.bst
- components/libvorbis.bst
- components/libvpx.bst
- components/opus.bst
- components/openjpeg.bst
- components/vulkan-icd-loader.bst
- components/v4l-utils.bst
- components/libwebp.bst
- components/xorg-lib-xcb.bst

variables:
  optimize-debug: "false"

  ffmpeg-prefix: '%{prefix}'
  ffmpeg-libdir: '%{libdir}'
  ffmpeg-arch: '%{arch}'
  (?):
  - target_arch == "i686":
      ffmpeg-arch: x86

  audio-encoders: |
    ac3,alac,flac,libfdk_aac,g723_1,mp2,libmp3lame,libopus,libspeex,pcm_alaw,pcm_mulaw,\
    pcm_f32le,pcm_s16be,pcm_s24be,pcm_s16le,pcm_s24le,pcm_s32le,\
    pcm_u8,tta,libvorbis,wavpack,%{extra-aud-enc}

  audio-decoders: |
    ac3,adpcm_g722,alac,flac,g723_1,g729,libfdk_aac,libopus,libspeex,\
    mp2,mp3,m4a,pcm_alaw,pcm_mulaw,pcm_f16le,pcm_f24le,pcm_f32be,\
    pcm_f32le,pcm_f64be,pcm_f64le,pcm_s16be,pcm_s16be_planar,pcm_s24be,\
    pcm_s16le,pcm_s16le_planar,pcm_s24le,pcm_s24le_planar,pcm_s32le,\
    pcm_s32le_planar,pcm_s64be,pcm_s64le,pcm_s8,pcm_s8_planar,\
    pcm_u8,pcm_u24be,pcm_u24le,pcm_u32be,pcm_u32le,tta,vorbis,wavpack,\
    %{extra-aud-dec}

  video-encoders: |
    ass,ffv1,libaom_av1,libvpx_vp8,libvpx_vp9,mjpeg_vaapi,rawvideo,\
    theora,vp8_vaapi,%{extra-vid-enc}

  video-decoders: |
    ass,ffv1,mjpeg,mjpegb,libaom_av1,libdav1d,libvpx_vp8,libvpx_vp9,\
    rawvideo,theora,vp8,vp9,%{extra-vid-dec}

  image-formats: |
    bmp,gif,jpegls,png,tiff,webp,%{extra-image-formats}

  conf-local: >-
    --prefix="%{ffmpeg-prefix}"
    --libdir="%{ffmpeg-libdir}"
    --optflags="${CFLAGS}"
    --extra-ldflags="${LDFLAGS}"
    --disable-stripping
    --disable-doc
    --disable-static
    --disable-encoders
    --disable-decoders
    --enable-shared
    --enable-gnutls
    --enable-gcrypt
    --enable-ladspa
    --enable-lcms2
    --enable-libaom
    --enable-libdav1d
    --enable-libfdk-aac
    --enable-libmp3lame
    --enable-libfontconfig
    --enable-libfreetype
    --enable-libfribidi
    --enable-libopus
    --enable-libpulse
    --enable-libspeex
    --enable-libtheora
    --enable-libvorbis
    --enable-libvpx
    --enable-librsvg
    --enable-libwebp
    --enable-libxml2
    --enable-libopenjpeg
    --enable-openal
    --enable-opengl
    --enable-sdl2
    --enable-vulkan
    --enable-zlib
    --enable-libv4l2
    --enable-libxcb
    --enable-vdpau
    --enable-vaapi
    --enable-pthreads
    --enable-encoder=%{audio-encoders}
    --enable-encoder=%{video-encoders}
    --enable-decoder=%{audio-decoders}
    --enable-decoder=%{video-decoders}
    --enable-encoder=%{image-formats}
    --enable-decoder=%{image-formats}
    --arch="%{ffmpeg-arch}"

  conf-extra: ''

  extra-aud-enc: ''
  extra-aud-dec: ''
  extra-vid-enc: ''
  extra-vid-dec: ''
  extra-image-formats: ''
  extra-hwaccels: ''

public:
  bst:
    split-rules:
      devel:
        (>):
        - '%{libdir}/libavdevice.so'
        - '%{libdir}/libavcodec.so'
        - '%{libdir}/libswscale.so'
        - '%{libdir}/libpostproc.so'
        - '%{libdir}/libavutil.so'
        - '%{libdir}/libavfilter.so'
        - '%{libdir}/libavformat.so'
        - '%{libdir}/libswresample.so'
        - '%{datadir}/ffmpeg/examples'
        - '%{datadir}/ffmpeg/examples/**'

# ffmpeg is not using autotools, but a configure and Makefile files
config:
  configure-commands:
  - ./configure %{conf-local} %{conf-extra}

sources:
- kind: git_repo
  url: ffmpeg:ffmpeg.git
  track: n*
  exclude:
  - '*-dev'
  ref: n6.0-0-gea3d24bbe3c58b171e55fe2151fc7ffaca3ab3d2
